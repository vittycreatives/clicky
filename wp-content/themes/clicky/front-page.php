<?php get_header(); ?>

<?php get_template_part( 'components/hero' ); ?>

<?php get_template_part( 'components/text' ); ?>

<?php get_template_part( 'components/person_slider' ); ?>

<?php get_template_part( 'components/accordion' ); ?>

<?php get_template_part( 'components/people' ); ?>

<?php get_template_part( 'components/contact' ); ?>

<?php get_template_part( 'components/enquiry' ); ?>

<?php get_footer(); ?>