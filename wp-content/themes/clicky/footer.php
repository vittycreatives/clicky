
		<footer>
			<div class="wrapper">
				<div class="row">
					<div class="lg-col-3 md-col-6">
						<h4>Link One</h4>
						<ul class="menu menu--footer">
							<li>Here is a long link</li>
							<li>Short link</li>
							<li>Short link</li>
						</ul>
					</div>
					<div class="lg-col-3 md-col-6">
						<h4>Link Two</h4>
						<ul class="menu menu--footer">
							<li>Here is a long link</li>
							<li>Short link</li>
							<li>Short link</li>
						</ul>
					</div>
					<div class="lg-col-3 md-col-6">
						<h4>Link Three</h4>
						<ul class="menu menu--footer">
							<li>Here is a long link</li>
							<li>Short link</li>
							<li>Short link</li>
						</ul>
					</div>
					<div class="lg-col-3 md-col-6">
						<h4>Link Newsletter Sign up</h4>
						<div class="footer_extend">
							<form class="newsletter" action="">
								<label>
									<input type="email" placeholder="Enter email*">
								</label>
							</form>
							<a class="footer_contact" href="tel:01234 567 890">01234 567 890</a>
							<a class="footer_contact" href="mailto:hello@company.co.uk">hello@company.co.uk</a>
						</div>
					</div>
				</div>
			</div>
			<div class="lower_bar">
				<div class="wrapper">
					<div class="row">
						<div class="lg-col-12">
							<p>Copyright Company Name | Website Design and Development by Clicky</p>
						</div>
					</div>
				</div>
			</div>
		</footer>

		

		<?php wp_footer(); ?>

	</body>
</html>
