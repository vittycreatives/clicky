jQuery(document).ready(function() {
//below here

	/*------------------------------------*\
		Mobile Menu
	\*------------------------------------*/
	jQuery(".mobile_btn .hamburger").click(function(e){
		jQuery('body').toggleClass('page_fixed');   
		jQuery(".hamburger").toggleClass("is-active");
		jQuery(".mobile_menu").toggleClass("active");
		e.preventDefault();
	});

	/*------------------------------------*\
		Mobile Menu
	\*------------------------------------*/
	jQuery('.person_slider').slick({
		infinite: true,
		centerMode: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		variableWidth: true,
		dots: true,
		arrows: false,
		responsive: [
	    {
			breakpoint: 900,
			settings: {
				variableWidth: false,
			}
	    },
	    {
	      	breakpoint: 600,
	      	settings: {
				variableWidth: false,
			}
	    }
	  ]
	});

	/*------------------------------------*\
		Accordion Menu
	\*------------------------------------*/
		
	jQuery(".accordion__single .single_question").click(function(e){
		jQuery(this).next('.single_answer').slideToggle();   
		// jQuery(this).find('.question_arrow').toggleClass("active");
		jQuery(this).find('.question_arrow--plus').toggle();
		jQuery(this).find('.question_arrow--minus').toggle();
		e.preventDefault();
	});

//above here
});

