<section class="enquiry">
    <div class="wrapper">
        <div class="row">
            <div class="lg-col-12">
                <h2><?php the_field('enquiry_heading'); ?></h2>
                <p><?php the_field('enquiry_text'); ?></p>
                <?php echo do_shortcode( get_field('enquiry_shortcode') ); ?>
            </div>
        </div>
    </div>
</section>