<section class="hero" style="background-image:url('<?php the_field('hero_background_image'); ?>')">
    <div class="overlay"></div>
    <div class="wrapper">
        <div class="row">
            <div class="lg-col-12">
                <div class="hero__content">
                    <h1><?php the_field('hero_heading'); ?></h1>
                    <p><?php the_field('hero_subheading'); ?></p>
                    <a href="<?php the_field('hero_button_url'); ?>" class="btn btn--white"><?php the_field('hero_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>