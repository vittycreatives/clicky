<section class="accordion_outer">
    <div class="wrapper">
        <div class="row">
            <div class="lg-col-12">
                <h2><?php the_field('accordion_heading'); ?></h2>

                <div class="accordion">

                    <?php if( have_rows('accordion') ): ?> 
                        <?php while( have_rows('accordion') ): the_row(); 
                            $question = get_sub_field('question');
                            $answer = get_sub_field('answer');
                        ?> 
                            <div class="accordion__single">
                                <div class="single_question">
                                    <?php echo $question; ?>
                                    <div class="question_arrow question_arrow--plus">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/cross.svg" alt="Plus">
                                    </div>
                                    <div class="question_arrow question_arrow--minus">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/minus.svg" alt="Minus">
                                    </div>
                                </div>
                                <div class="single_answer">
                                    <p><?php echo $answer; ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>        
                    <?php endif; ?> 
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
