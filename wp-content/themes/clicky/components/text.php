<section class="text_area">
    <div class="wrapper">
        <div class="row">
            <div class="lg-col-12">
                <div class="text_wrapper">
                    <div class="text_txt">
                        <div class="text_inner">
                            <h2><?php the_field('text_heading'); ?></h2>
                            <?php the_field('text_area'); ?>
                        </div>  
                    </div>
                    <div class="text_img">
                        <img src="<?php the_field('text_image'); ?>" alt="<?php the_field('text_heading'); ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>