<section class="people">
    <div class="wrapper">
        <div class="row">
            <div class="lg-col-12">

                <?php if( have_rows('people') ): ?> 
                    <?php while( have_rows('people') ): the_row(); 
                        $image = get_sub_field('image');
                        $text = get_sub_field('text');
                    ?> 
                        <div class="people__block">
                            <div class="block_img">
                                <div class="img">
                                    <img src="<?php echo $image; ?>" alt="Person">
                                </div>
                            </div>
                            <div class="block_txt">
                                <p><?php echo $text; ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>        
                <?php endif; ?> 
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>