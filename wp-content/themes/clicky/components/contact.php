<section class="contact_us">
    <div class="wrapper">
        <div class="row">
            <div class="lg-col-12">
                <h2>Contact Us</h2>

                <div class="contact_wrapper">
                    <div class="contact">
                        <div class="contact__details">
                            <div class="details_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/phone.svg" alt="Call Us">
                            </div>
                            <div class="details_txt">
                                <p class="small">Call Us</p>
                                <p><?php the_field('phone_number'); ?></p>
                            </div>
                        </div>
                        <div class="contact__details">
                            <div class="details_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/letter.svg" alt="Email Us">
                            </div>
                            <div class="details_txt">
                                <p class="small">Email Us</p>
                                <p><?php the_field('email_address'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="contact">
                        <div class="contact__details contact__details--single">
                            <div class="details_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/house.svg" alt="Our Address">
                            </div>
                            <div class="details_txt">
                                <p>
                                    <?php the_field('address'); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>