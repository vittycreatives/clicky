<section class="person_slider">
    
    <?php if( have_rows('slider') ): ?> 
        <?php while( have_rows('slider') ): the_row(); 
            $background_image = get_sub_field('background_image');
            $person_image = get_sub_field('person_image');
            $text = get_sub_field('text');
            $name_role = get_sub_field('name_role');
        ?>  
            <div class="slide" style="background-image: url('<?php echo $background_image; ?>');">
                <div class="overlay"></div>
                <div class="slide__img">
                    <img src="<?php echo $person_image; ?>" alt="NAME / ROLE / COMPANY">
                </div>
                <div class="slide__text">
                   <p><?php echo $text; ?></p>
                </div>
                <div class="slide__name">
                    <p><?php echo $name_role; ?></p>
                </div>
            </div>
        <?php endwhile; ?>        
    <?php endif; ?> 
    <?php wp_reset_query(); ?>

</section>