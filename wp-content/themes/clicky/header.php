<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>

		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.svg" type="image/x-icon">
		<!-- For third-generation iPad with high-resolution Retina display: -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.svg">
		<!-- For iPhone with high-resolution Retina display: -->
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.svg">
		<!-- For first- and second-generation iPad: -->
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.svg">
		<!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.svg">		

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>		

		<div class="mobile_menu">
			<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_class' => 'menu menu--header' ) ); ?>
		</div>

		<header>
			<div class="wrapper">
				<div class="row">
					<div class="lg-col-3 md-col-6 sm-col-8">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" id="logo" alt="<?php bloginfo('name'); ?>"></a>
					</div>
					<div class="lg-col-9 md-col-6 sm-col-4">
						<nav>
							<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_class' => 'menu menu--header' ) ); ?>
						</nav>
						<div class="mobile_btn">
							<span class="hamburger hamburger--spring js-hamburger" type="button">
						  		<span class="hamburger-box">
							    	<span class="hamburger-inner"></span>
						  		</span>
							</span>	
						</div>
					</div>
				</div>
				<div class="row">
					<div class="lg-col-12">
						<hr class="hr hr--blue">
					</div>
				</div>
			</div>
		</header>