<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'clicky');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');
define('WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);
define('WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'iLbM2iFHDR0CQ0cVU1f1s7wWtR3UPwe0yioUFeaup6ICap7JgCtyDDEv2EeBWRGL');
define('SECURE_AUTH_KEY',  'cSzSlV0BVYjtvVetrVq7H2opW0V4g26jN0A8ESUM8uhm2gFnIu904JhOksE05uoj');
define('LOGGED_IN_KEY',    'HinzZyDdXzvIrQaPXAO3qvixVpCKPSZr7KaFOstdX7pe08V8ZHTXXj4c9jG1cYOT');
define('NONCE_KEY',        '0Jry3ioyVedOzaa1kbAbBZO2OWMZ7ZssjfjNkMeXzZEcmqWRu0dnUR6TJe7mWEID');
define('AUTH_SALT',        'h6iToKNbSWyLYtmXAWV3b4Y78P9FdrCtfYp7qX9edscEO0BvvOF6wTwlTe30UcLt');
define('SECURE_AUTH_SALT', 'PmsDWwZk1GCCCT0fiYJEVw60bjLbbIK2fUz8zTFiW4tb5hDM80jWeIwbxJJeIvw4');
define('LOGGED_IN_SALT',   'KyHfMzrGJOVAzF9JFVjoymEpklBoDcjAVPNmSFKjNjnuB5h7AouV12eaxdOmMoYO');
define('NONCE_SALT',       'M6j8dcfTFzNXCj4HQv7CLHy70VzVUvy4fGPFKcBOG1m5OWqTN3gPpMHzwqnEAbX5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WPCF7_AUTOP', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
